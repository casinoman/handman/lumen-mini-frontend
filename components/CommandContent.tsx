type Props = {
  content: string;
};

const CommandContent = (props: Props) => {
  const { content } = props;

  return <main className="text-justify">{content}</main>;
};

export default CommandContent;
