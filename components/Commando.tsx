import CommandHeader from "components/CommandHeader";
import CommandContent from "components/CommandContent";
import CommandActions from "components/CommandActions";

type Props = {
  Command: Command;
};

const Commando = (props: Props) => {
  const { Command } = props;

  return (
    <div className="flex-grow py-24 flex flex-col gap-24 justify-between items-center">
      <div className="px-24 flex flex-col gap-24 w-[54rem]">
        <CommandHeader
          title={Command.title}
          author={Command.author}
          createdAt={Command.created_at}
          updatedAt={Command.updated_at}
        />
        <CommandContent content={Command.content} />
      </div>
      <CommandActions id={Command.id} />
    </div>
  );
};

export default Commando;
