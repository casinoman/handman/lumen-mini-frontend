import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPlus } from "@fortawesome/free-solid-svg-icons";
import Link from "next/link";
import { useEffect, useState } from "react";

import CommandPreview from "components/CommandPreview";
import Loader from "components/Loader";

import sleep from "utils/sleep";

const CommandList = () => {
  const [Commands, setCommands] = useState<Array<Command>>([]);

  useEffect(() => {
    const fetchCommands = async () => {
      const response = await fetch("/api/commando", {
        method: "GET",
      });

      await sleep(100);

      if (response.ok) {
        const data: Array<Command> = await response.json();
        setCommands(data);
      }
    };

    fetchCommands();
  }, []);

  if (Commands.length === 0) {
    return <Loader />;
  }

  return (
    <div className="min-h-screen p-24 flex flex-col gap-8 animate-fade-in-up items-center">
      <Link href="/create">
        <a className="w-fit">
          <button className="btn gap-2">
            <FontAwesomeIcon icon={faPlus} className="text-primary" />
            Manual Commando
          </button>
        </a>
      </Link>
      {Commands.map((Command) => (
        <CommandPreview key={Command.id} Command={Command} />
      ))}
    </div>
  );
};

export default CommandList;
