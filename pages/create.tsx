import { NextPage } from "next";

import CreateCommand from "components/CreateCommand";

const CreatePage: NextPage = () => {
  return (
    <div className="min-h-screen p-24 flex flex-col justify-between animate-fade-in-up">
      <CreateCommand />
    </div>
  );
};

export default CreatePage;
