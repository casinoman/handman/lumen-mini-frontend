import { NextPage } from "next";

import CreateLinkRequest from "components/CreateLinkRequest";

const LinkPage: NextPage = () => {
  return (
    <div className="min-h-screen p-24 flex flex-col justify-between animate-fade-in-up">
      <CreateLinkRequest />
    </div>
  );
};

export default LinkPage;
